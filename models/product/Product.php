<?php

namespace app\models\product;


use app\components\cart\product\ShoppingProduct;

class Product extends \app\models\activerecord\product\Product implements ShoppingProduct
{
	public $discount = 0.00;
	public function getPriceWithDiscount()
	{
		if ($this->discount !== null) {
			return $this->price - $this->discount;
		}

		return $this->price;
	}
}