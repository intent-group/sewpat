<?php

namespace app\controllers;


use app\models\user\form\SignUpForm;
use app\service\UserService;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class SignUpFormController extends  Controller
{
	/**
	 * @return array
	 */
	public function behaviors() :array
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'final-up' => ['get']
				],
			],
		];
	}

	/**
	 * @return string|\yii\web\Response
	 * @throws \yii\base\Exception
	 */
	public function actionIndex()
	{
		$signUpForm = UserService::getSignUpForm();

		/** @var SignUpForm $signUpForm */
		if ($signUpForm->load(\Yii::$app->request->post()) && $signUpForm->signUp()) {
			return $this->goHome();
		}

		return $this->render('index', [
			'model' => $signUpForm
		]);
	}

	public function actionFinalUp($code)
	{
		if (UserService::checkCode($code)) {
			return $this->redirect(Url::to(['profile/user/update', 'id' => \Yii::$app->user->identity->getId()]));
		}

		\Yii::$app->session->setFlash('badCode', 'This link is not valid');

		return $this->render('final-up');
	}
}