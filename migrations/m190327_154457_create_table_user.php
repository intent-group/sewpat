<?php

use yii\db\Migration;

/**
 * Class m190327_154457_create_table_user
 */
class m190327_154457_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255)->notNull(),
			'surname' => $this->string(255)->notNull(),
			'phone' => $this->string(255)->notNull(),
			'avatar' => $this->string(255),
			'username' => $this->string(255),
			'about_me' => $this->text()->defaultValue(null),
			'email' => $this->string(255)->notNull(),
			'password_hash' => $this->string(255),
			'country_id' => $this->integer(11),
			'city_id' => $this->integer(11),
			'access_token' => $this->string(255),
			'auth_key' => $this->string(255),
			'is_malling' => $this->integer(1)->defaultValue(0),
			'is_confirm' => $this->integer(1)->defaultValue(0),
			'created_at' => $this->date()->notNull(),
			'update_at' => $this->date(),
			//TODO: add column for identity interface
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('user');
    }
}
