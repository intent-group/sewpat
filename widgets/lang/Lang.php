<?php


namespace app\widgets\lang;


use yii\base\Widget;

class Lang extends Widget
{
	public function init(){}

	public function run() {
		return $this->render('index', [
			'current' => \app\models\lang\Lang::getCurrent(),
			'langs' => \app\models\lang\Lang::find()->where('id != :current_id', [':current_id' => \app\models\lang\Lang::getCurrent()->id])->all(),
		]);
	}
}