<?php

namespace app\components\cart\calculator\base;


use app\components\cart\product\ShoppingProduct;

/**
 * Interface Calculate
 * @package app\components\cart\calculator\base
 */
interface Calculate
{
	/**
	 * @param ShoppingProduct $item
	 *
	 * @return mixed
	 */
	public static function plus(ShoppingProduct $item);

	/**
	 * @param ShoppingProduct $item
	 *
	 * @return mixed
	 */
	public static function minus(ShoppingProduct $item);

	/**
	 * @return mixed
	 */
	public static function getSum();

	/**
	 * @return mixed
	 */
	public static function resetAmount();

}