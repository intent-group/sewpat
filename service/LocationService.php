<?php

namespace app\service;


use app\models\country\Country;
use yii\helpers\ArrayHelper;

class LocationService
{
	/**
	 * @param null $id
	 *
	 * @return Country|null
	 */
	public static function get($id = null): ?Country
	{
		if ($id !== null) {
			return Country::findOne(['id' => $id]);
		}

		return new Country();
	}

	/**
	 * @return array $countries
	 */
	public static function countriesToArray(): array
	{
		return ArrayHelper::map(Country::find()->all(), 'id', 'name');
	}

}