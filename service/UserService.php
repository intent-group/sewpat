<?php

namespace app\service;


use app\models\user\form\SignUpForm;
use app\models\user\User;

class UserService
{
	/**
	 * @param null $id
	 *
	 * @return User|null
	 */
	public static function get($id = null): ?User
	{
		if ($id !== null) {
			return User::findOne(['id' => $id]);
		}

		return new User();
	}

	/**
	 * @return SignUpForm
	 */
	public static function getSignUpForm(): SignUpForm
	{
		return new SignUpForm();
	}

	/**
	 * @param $code
	 *
	 * @return bool
	 */
	public static function checkCode($code): bool
	{
		$user = self::get(\Yii::$app->user->identity->getId());

		return $user->auth_key === $code;
	}
}