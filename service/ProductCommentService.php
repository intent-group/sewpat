<?php

namespace app\service;


use app\models\pucomment\Comment;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;

class ProductCommentService
{

	/**
	 * @param null $id
	 *
	 * @return \app\models\activerecord\pucomment\CommentQuery|Comment
	 */
	public static function get($id = null)
	{
		if ($id !== null) {
			return Comment::findOne(['id' => $id]);
		}

		return new Comment();
	}

	/**
	 * @param $comment
	 *
	 * @return bool
	 */
	public static function isParent(Comment $comment): bool
	{
		$comments = Comment::find()->where(['parent_id' => $comment->id])->all();

		return $comments === null;
	}

	/**
	 * @param Comment $comment
	 *
	 * @return ActiveDataProvider
	 * @throws ErrorException
	 */
	public static function getChildren(Comment $comment): ActiveDataProvider
	{
		if (!self::isParent($comment)) {
			$dataProvider = new ActiveDataProvider([
				'query' => Comment::find()->where(['parent_id' => $comment->id])
			]);

			return $dataProvider;
		}

		throw new ErrorException('This comment its parent');
	}
}