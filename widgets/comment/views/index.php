<?php
use yii\widgets\ListView;


/* @var $this \yii\web\View */
/* @var $dataProvider  */
/* @var $id string */
/* @var $parent_id string */

echo ListView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_comment',
	'viewParams' => [
		'id' => $id,
		'parent_id' => $parent_id
	]
]);


?>


