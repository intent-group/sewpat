<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
//var_dump($model->category_id->all());
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'images[]')->widget(FileInput::class, [
		'options' => [
				'accept' => 'image/*',
			    'multiple' => true,

			],
		'pluginOptions' => [
			'initialPreview'=> \app\service\ProductService::getGallery($model->id),

			'overwriteInitial' => true,
			'maxFileCount' => 10,
		]
	]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->widget(Widget::className(), [
	    'settings' => [
		    'lang' => 'ru',
		    'minHeight' => 200,
		    'plugins' => [
			    'clips',
			    'fullscreen',
		    ],
		    'clips' => [
			    ['Lorem ipsum...', 'Lorem...'],
			    ['red', '<span class="label-red">red</span>'],
			    ['green', '<span class="label-green">green</span>'],
			    ['blue', '<span class="label-blue">blue</span>'],
		    ],
	    ],
    ]); ?>


	<?= $form->field($model, 'category_id')->widget(Select2::class, [
		'data' => \app\service\CategoryService::toArray(),
		'language' => 'ru',
		'value' => $model->category_id,
		'options' => ['placeholder' => 'Select a state ...'],
		'pluginOptions' => [
			'allowClear' => true,
			'multiple' => true,
			'tags' => true,
		],
	]); ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput()->dropDownList(['0' => 'Нет', '1' => 'Да']) ?>

	<?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'seo_url')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'seo_keyword')->widget(Select2::class, [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\activerecord\keyword\Keyword::find()->all(), 'id', 'name') ,
    'language' => 'ru',
    'value' => $model->seo_keyword,
    'options' => ['placeholder' => 'Select a state ...'],
    'pluginOptions' => [
        'allowClear' => true,
	    'multiple' => true,
	    'tags' => true,
    ],
		]); ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
