<?php

namespace app\controllers;


use app\models\product\Product;
use app\service\ProductService;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class ProductController extends Controller
{
	/**
	 * @return string
	 */
	public function actionIndex(): string
	{
		$product = Product::find()->all();

		return $this->render('index', [
			'models' => $product
		]);
	}

	public function actionView($id)
	{
		$product = ProductService::get($id);

		$dataProvider = new ActiveDataProvider([
			'query' => \app\models\pucomment\Comment::find()->where(['parent_id' => 0, 'product_id' => $product->id]),
		]);

		return $this->render('view', [
			'product' => $product,
			'dataProvider' => $dataProvider,
		]);
	}
}