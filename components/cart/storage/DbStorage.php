<?php

namespace app\components\cart\storage;


use app\components\cart\storage\base\Storage;
use yii\base\BaseObject;
use yii\db\Exception;

/**
 * Class DbStorage
 * @package app\components\cart\storage
 */
class DbStorage extends BaseObject implements Storage
{
	private $status;

	public $items;

	private $user;

	public $table;

	public function changeStatus($newStatus)
	{

	}

	/**
	 * @param $item
	 *
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function insert($item)
	{
		if ($this->isIsset($item)) {
			return false;
		}

		\Yii::$app->db->createCommand()->insert($this->table, [
			'product_id' => $item->id,
			'user_id' => $this->user,
			'sum' => $item->getPriceWithDiscount(),
		])->execute();

		return true;
	}

	/**
	 * @param $item
	 *
	 * @return mixed|void
	 */
	public function delete($item)
	{
		try {
			\Yii::$app->db->createCommand()->delete($this->table, [
				'product_id' => $item->id,
				'user_id' => $this->user,
			])->execute();
		} catch (Exception $e) {
			echo 'This error ' . $e;
		}
	}

	/**
	 * @return int|mixed
	 */
	public function getIdStorage()
	{
		return 1;
	}

	/**
	 * @return array|mixed
	 * @throws Exception
	 */
	public function getAll()
	{
		$items = [];
		if ($this->user !== null) {
			$items = \Yii::$app->db->createCommand('SELECT * FROM ' . $this->table . ' WHERE user_id= :user')
				->bindValue(':user', $this->user)
				->queryAll();
		}

		return $items;
	}

	/**
	 * @return bool
	 */
	public function clearAll(): bool
	{
		try {
			\Yii::$app->db->createCommand()->delete($this->table, [
				'user_id' => $this->user,
			])->execute();
		} catch (Exception $e) {
			echo $e;
		}

		return true;
	}

	/**
	 * @param $item
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function isIsset($item): bool
	{
		$query = \Yii::$app->db
			->createCommand('SELECT * FROM '  . $this->table . ' WHERE product_id=' . $item->id . ' AND  user_id=' . " '$this->user'")
			->execute();

		return $query !== 0;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user): void
	{
		if (is_string($this->user)) {
			$this->user = "'$user'";
		} else {
			$this->user = $user;
		}
	}
}