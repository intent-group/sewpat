<?php

namespace app\widgets\comment;


use yii\base\ErrorException;
use yii\base\Widget;

class Comment extends Widget
{
	public $dataProvider;
	public $attrId = 'id';
	public $attrParentId = 'parent_id';


	public function init()
	{
		parent::init();

		if ($this->dataProvider === null) {
			throw new ErrorException('This params $dataProvider is null');
		}
	}

	public function run()
	{
		return $this->render('index', [
			'dataProvider' => $this->dataProvider,
			'id' => $this->attrId,
			'parent_id' => $this->attrParentId
		]);
	}
}