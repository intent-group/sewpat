<?php

namespace app\models\user\form;


use app\service\MailService;
use app\service\UserService;
use yii\base\Model;

/**
 * Class SignUpForm
 * @package app\models\user\form
 */
class SignUpForm extends Model
{
	public $name;
	public $surname;
	public $username;
	public $email;
	public $password_hash;
	public $repeat_password;
	public $country_id;
	public $access_token;
	public $is_malling;
	public $is_confirm;
	public $at_created;
	public $reCaptcha;
	public $auth_key;


	/**
	 * @return array
	 */
	public function rules(): array
	{
		return [
			[['name', 'surname', 'username', 'email', 'password_hash', 'repeat_password', 'is_malling', 'country_id'], 'required' ],
			[['is_malling', 'is_confirm', 'country_id'], 'integer'],
			[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::class, 'secret' => '6Lcw55oUAAAAAJYSVy8U1ASAwxZKjyDAGoxcQ4Bb', 'uncheckedMessage' => 'Please confirm that you are not a bot.'],
			[['name', 'surname', 'username', 'email'], 'string' ],
			[['password_hash', 'repeat_password'], 'string', 'min' => 6 ],
			[['is_confirm'], 'default', 'value' => 0 ],
			['email', 'email'],
		];
	}

	/**
	 * @return bool
	 * @throws \yii\base\Exception
	 */
	public function signUp(): bool
	{
		$this->at_created = date('Y-m-d');
		if ($this->validatePassword() === false) {
			\Yii::$app->session->setFlash('errorSignUp', 'Password is not valid');
			return false;
		}

		$this->auth_key = \Yii::$app->security->generateRandomString();
		$vars = ['User' => $this->attributes];
		$user = UserService::get();

		if ($user !== null && $user->load($vars) && $user->save()) {
			MailService::sendCodeSignUp($this->auth_key);
			return \Yii::$app->user->login($user);
		}

		$this->password_hash = '';
		$this->repeat_password = '';
		\Yii::$app->session->setFlash('errorSignUp', 'Error registration');

		return false;
	}

	/**
	 * @throws \yii\base\Exception
	 */
	private function validatePassword(): bool
	{
		if ($this->password_hash === $this->repeat_password) {
			return $this->setPasswordHash($this->password_hash);
		}

		return false;
	}

	/**
	 * @param mixed $password_hash
	 *
	 * @return bool
	 * @throws \yii\base\Exception
	 */
	private function setPasswordHash($password_hash): bool
	{
		$this->password_hash = \Yii::$app->security->generatePasswordHash($password_hash);

		return true;
	}
}