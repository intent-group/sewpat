<?php

namespace app\components;

use app\models\lang\Lang;
use yii\web\UrlManager;

class LangUrlManager extends UrlManager
{
	public function createUrl($params)
	{
		if (isset($params['lang_id'])) {
			$lang = Lang::findOne($params['lang_id']);
			if ($lang === null) {
				$lang = Lang::getDefaultLang();
			}

			unset($params['lang_id']);
		} else {
			$lang = Lang::getCurrent();
		}

		$url = parent::createUrl($params);

		return $url == '/' ? '/' . $lang->url : '/' . $lang->url . $url;
	}
}