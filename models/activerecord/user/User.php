<?php

namespace app\models\activerecord\user;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $avatar
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property int $country_id
 * @property string $access_token
 * @property string $auth_key
 * @property int $is_malling
 * @property int $is_confirm
 * @property string $at_created
 * @property string $at_update
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'at_created'], 'required'],
            [['country_id', 'is_malling', 'is_confirm'], 'integer'],
            [['at_created', 'at_update'], 'safe'],
            [['name', 'surname', 'avatar', 'username', 'email', 'password_hash', 'access_token', 'auth_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'avatar' => Yii::t('app', 'Avatar'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'country_id' => Yii::t('app', 'Country ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'is_malling' => Yii::t('app', 'Is Malling'),
            'is_confirm' => Yii::t('app', 'Is Confirm'),
            'at_created' => Yii::t('app', 'At Created'),
            'at_update' => Yii::t('app', 'At Update'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
