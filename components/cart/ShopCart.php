<?php

namespace app\components\cart;


use app\components\cart\storage\base\Storage;
use yii\base\Component;


/**
 * Class ShopCart
 * @package app\components\cart
 */
class ShopCart extends Component
{
	/**
	 * @var $items Product[] all product in shop cart
	 */
	private $items;

	/**
	 * @var $totalCount this is counter for items
	 */
	private $totalCount;

	/**
	 * @var $totalPrice this full price for order
	 */
	private $totalPrice;

	/**
	 * @var $productClass this class implements
	 * Shopping product interface for checkout item
	 */
	public $productClass;

	/**
	 * @var $calculateClass this class implements
	 *  Calculate interface for counting product data
	 */
	public $calculateClass;

	/**
	 * @var $storageClass his class implements
	 * Storage interface to store and retrieve information
	 * about the stored data in the cart
	 */
	public $storageClass;

	/**
	 * @var $userId current authorized in the system or session user
	 */
	public $userId;

	/**
	 * @var $table string indicates on table for stored table
	 */
	public $table;

	/**
	 * Initializes this object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init(): void
	{
		parent::init();

		$this->userId = \Yii::$app->user->id === null ?
			\Yii::$app->session->id :
			\Yii::$app->user->id;

		$data = $this->storage->all;

		foreach ($data as $row) {
			$this->items[] = $this->fetchItem($row['product_id'])[0];
		}

		$this->totalPrice = $this->calculateClass::totalPrice($this->items);
		$this->totalCount = $this->calculateClass::totalCount($this->items);
	}

	/**
	 * @return $this
	 */
	public function getBasket(): self
	{
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAllItems(): array
	{
		return $this->items;
	}

	/**
	 * @return mixed
	 */
	public function emptyShoppingCart()
	{
		return $this->storage->clearAll();
	}

	/**
	 * @param $ids
	 *
	 * @return bool
	 * @throws \ErrorException
	 */
	public function addItem(array $ids): bool
	{
		if ($ids === null) {
			throw new \ErrorException('Missing params ids');
		}

		$items = $this->fetchItem($ids);

		if ($this->setData($items)) {

			return true;
		}

		return false;
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public function removeItem($id): bool
	{
		$data = $this->getKeyForItem($id);

		$this->unsetData($data['item'], $data['key']);

		return true;
	}

	/**
	 * @param $item
	 * @param $key
	 *
	 * @return bool
	 */
	private function unsetData($item, $key): bool
	{
		if ($this->storage->delete($item) && $this->calculateClass::minus($item)) {
			unset($this->items[$key]);

			return true;
		}

		return false;
	}

	/**
	 * @param $items
	 *
	 * @return bool
	 */
	private function setData(array $items): bool
	{
		if ($items !== null) {
			foreach ($items as $item) {
				if ($this->storage->insert($item)) {
					$this->calculateClass::plus($item);
				}
			}

			$this->totalPrice = $this->calculateClass::getSum();

			return true;
		}

		return false;
	}

	/**
	 * @param $id
	 *
	 * @return array|bool
	 */
	private function getKeyForItem(int $id): ?array
	{
		foreach ($this->items as $key => $item) {
			if ($item->id === $id) {
				return ['item' => $item, 'key' => $key];
			}
		}

		return false;
	}

	/**
	 * @param $ids
	 *
	 * @return array
	 */
	private function fetchItem($ids): array
	{
		return $this->productClass::find()->where(['id' => $ids])->all();
	}

	/**
	 * @return mixed
	 */
	public function getStorage(): Storage
	{
		if ($this->storageClass instanceof Storage) {
			return $this->storageClass;
		}

		$this->storageClass = new $this->storageClass([
			'user' => $this->userId,
			'table' => $this->table,
		]);

		return $this->storageClass;
	}

	/**
	 * @return array
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}

