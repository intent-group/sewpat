<?php

namespace app\models\user;


use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models\user
 */
class User extends \app\models\activerecord\user\User implements IdentityInterface
{

	/**
	 * @param mixed $token
	 * @param null $type
	 *
	 * @return User|null|IdentityInterface
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne([['access_token' => $token ]]);
	}

	/**
	 * @param int|string $id
	 *
	 * @return User|null|IdentityInterface
	 */
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id]);
	}

	/**
	 * @return int|string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string current user auth key
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @param string $authKey
	 * @return bool if auth key is valid for current user
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * @param $email
	 *
	 * @return User|null
	 */
	public static function findByEmail($email): ?User
	{
		return self::findOne(['email' => $email]);
	}

	/**
	 * @return bool
	 */
	public function beforeDelete(): bool
	{
		if ($this->avatar !== null && is_file('image/' . $this->avatar)) {
			unlink('image/'  . $this->avatar);
		}

		return parent::beforeDelete();
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password): bool
	{
		return \Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
	}
}