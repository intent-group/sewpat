<?php

namespace app\widgets\cart;


use yii\base\Widget;


class CartWidget extends Widget
{
	public $id;

	public $option;

	public $text = 'buy';

	/**
	 * @throws \ErrorException
	 */
	public function init()
	{
		parent::init();


		if ($this->id !== null) {
			$this->option['data-product'] = $this->id;
			$this->option['class'] = 'btn btn-success add-to-cart';
		} else {
			throw new \ErrorException('Param id is empty');
		}
	}

	public function run(): string
	{
		return $this->render('button', [
			'text' => $this->text,
			'option' => $this->option,
		]);
	}

}