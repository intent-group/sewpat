<?php

namespace app\components\cart\storage\base;


interface Storage
{
	/**
	 * @param $item
	 *
	 * @return mixed
	 */
	public function insert($item);

	/**
	 * @param $item
	 *
	 * @return mixed
	 */
	public function delete($item);

	/**
	 * @param $newStatus
	 *
	 * @return mixed
	 */
	public function changeStatus($newStatus);

	/**
	 * @return mixed
	 */
	public function getIdStorage();

	/**
	 * @return mixed
	 */
	public function getAll();
}