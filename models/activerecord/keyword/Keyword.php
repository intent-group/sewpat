<?php

namespace app\models\activerecord\keyword;

use Yii;

/**
 * This is the model class for table "keyword".
 *
 * @property int $id
 * @property string $name
 *
 * @property ProductSeoKeyword[] $productSeoKeywords
 * @property ProductSeo[] $productSeos
 */
class Keyword extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keyword';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSeoKeywords()
    {
        return $this->hasMany(ProductSeoKeyword::className(), ['keyword_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSeos()
    {
        return $this->hasMany(ProductSeo::className(), ['id' => 'product_seo_id'])->viaTable('product_seo_keyword', ['keyword_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductSeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductSeoQuery(get_called_class());
    }
}
