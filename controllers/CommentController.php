<?php

namespace app\controllers;


use app\service\ProductCommentService;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

class CommentController extends Controller
{
	public function actionCreate()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => \app\models\pucomment\Comment::find()->where(['parent_id' => 0]),
		]);

		if (\Yii::$app->request->isPjax) {
			$data = \Yii::$app->request->post();
			$comment = ProductCommentService::get();
			$comment->load($data);
			 $comment->save();
		}

		return \app\widgets\comment\Comment::widget(['dataProvider' => $dataProvider]);
	}
}