<?php

/* @var $this \yii\web\View */
/* @var $model \app\models\user\form\SignUpForm */
?>

<?php if( Yii::$app->session->hasFlash('errorSignUp') ): ?>
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo Yii::$app->session->getFlash('errorSignUp'); ?>
	</div>
<?php endif;?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-lg-6">
				<?php $form = \yii\widgets\ActiveForm::begin()?>

				<?= $form->field($model, 'name')->textInput() ?>

				<?= $form->field($model, 'surname')->textInput() ?>

				<?= $form->field($model, 'username')->textInput() ?>

				<?= $form->field($model, 'email')->textInput() ?>

				<?= $form->field($model, 'password_hash')->textInput()->passwordInput() ?>

				<?= $form->field($model, 'repeat_password')->textInput()->passwordInput() ?>

				<?= $form->field($model, 'country_id')->textInput()->dropDownList(\app\service\LocationService::countriesToArray()) ?>

				<?= $form->field($model, 'is_malling')->checkbox() ?>

				<?= $form->field($model, 'reCaptcha')->widget(
					\himiklab\yii2\recaptcha\ReCaptcha::className(),
					['siteKey' => '6Lcw55oUAAAAAEdXk-8X07rVkNq8VOXNkrkO6C_U']
				) ?>

				<?= \yii\helpers\Html::submitButton('Sign Up', ['class' => 'btn btn-success']) ?>

				<?php \yii\widgets\ActiveForm::end() ?>
			</div>
			<div class="col-lg-6">

			</div>
		</div>
	</div>
</div>

