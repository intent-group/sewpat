<?php

/* @var $this \yii\web\View */
/* @var $product \app\models\product\Product|null */
/* @var $dataProvider \app\controllers\ActiveDataProvider */

$model = \app\service\ProductCommentService::get();
?>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-lg-6" style="height: 100%; border-right: black solid 2px">
				<?php foreach ($product->gallery->images as $value): ?>
					<div class="col-lg-6" >
						<?= \yii\helpers\Html::img('/image/gallery/' . $value->path, ['class' => 'img-responsive'] ) ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-lg-6">
				<ul>
					<li>
						title: <?= $product->title ?>
					</li>
					<li>
						description: <?= $product->description ?>
					</li>
					<li>
						price: <?= $product->price ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<?php \yii\widgets\Pjax::begin(['enablePushState' => false]) ?>
		<?= \app\widgets\comment\Comment::widget(['dataProvider' => $dataProvider]) ?>
		<div class="container">
			<?php $form = \yii\widgets\ActiveForm::begin(['action' => \yii\helpers\Url::to(['comment/create'])]) ?>

			<?= $form->field($model, 'title')->textInput() ?>

			<?= $form->field($model, 'message')->textarea() ?>

			<?= $form->field($model, 'parent_id')->hiddenInput()->label(false) ?>
			<?= \yii\helpers\Html::submitButton('Submit')?>
			<?php  \yii\widgets\ActiveForm::end() ?>

		<?php \yii\widgets\Pjax::end() ?>
		</div>
	</div>


</div>
