<?php

use yii\db\Migration;

/**
 * Class m190329_153325_create_table_country
 */
class m190329_153325_create_table_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('country', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255),
			'code' => $this->string(100),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('country');
    }
}
