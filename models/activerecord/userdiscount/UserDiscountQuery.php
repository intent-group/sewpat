<?php

namespace app\models\activerecord\userdiscount;

/**
 * This is the ActiveQuery class for [[UserDiscount]].
 *
 * @see UserDiscount
 */
class UserDiscountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserDiscount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserDiscount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
