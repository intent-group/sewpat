<?php

/* @var $this \yii\web\View */
?>


<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php if( Yii::$app->session->hasFlash('badCode') ): ?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo Yii::$app->session->getFlash('badCode'); ?>
				</div>
			<?php endif;?>
		</div>
	</div>
</div>