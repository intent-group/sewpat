<?php

namespace app\models\lang;


class Lang extends \app\models\activerecord\lang\Lang
{

	public static $current = null;

	public static function getCurrent()
	{
		if (self::$current === null) {
			self::$current = self::getDefaultLang();
		}
		return self::$current;
	}

	public static function setCurrent($url = null)
	{
		$language = self::getLangByUrl($url);
		self::$current = ($language === null) ? self::getDefaultLang() : $language;
		\Yii::$app->language = self::$current->local;
	}

	public static function getDefaultLang()
	{
		return Lang::find()->where('`default` = :default', [':default' => 1])->one();
	}

	public static function getLangByUrl($url = null)
	{
		if ($url === null) {
			return null;
		} else {
			$language = Lang::find()->where('url = :url', [':url' => $url])->one();
			if ($language === null) {
				return null;
			} else {
				return $language;
			}
		}
	}
}