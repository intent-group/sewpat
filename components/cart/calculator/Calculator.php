<?php

namespace app\components\cart\calculator;

use app\components\cart\calculator\base\Calculate;
use app\components\cart\product\ShoppingProduct;
use yii\base\BaseObject;

/**
 * Class Calculator
 * @package app\components\cart\calculator
 */
class Calculator extends BaseObject implements Calculate
{
	private static $sum = 0.00;

	/**
	 * @param \app\components\cart\product\ShoppingProduct $item
	 *
	 * @return bool
	 */
	public static function plus(ShoppingProduct $item): bool
	{
		$price = $item->getPriceWithDiscount();

		self::$sum += $price;

		return true;
	}

	/**
	 * @param $items
	 *
	 * @return float
	 */
	public static function totalPrice($items): float
	{
		if ($items !== null) {
			foreach ($items as $key => $item) {
				self::plus($item);
			}
		}

		return self::$sum;
	}

	/**
	 * @param $items
	 *
	 * @return int
	 */
	public static function totalCount($items): int
	{
		return $items === null ? 0 : count($items);
	}

	/**
	 * @param \app\components\cart\product\ShoppingProduct $item
	 *
	 * @return bool
	 */
	public static function minus($item)
	{
		$price = $item->getPriceWithDiscount();

		self::$sum -= $price;

		return true;
	}

	/**
	 * @return float
	 */
	public static function getSum()
	{
		return self::$sum;
	}

	/**
	 * @return float
	 */
	public static function resetAmount()
	{
		return self::$sum = 0.00;
	}
}