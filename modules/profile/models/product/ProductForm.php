<?php

namespace app\modules\profile\models\product;


use app\models\activerecord\keyword\Keyword;
use app\models\activerecord\productseokeyword\ProductSeoKeyword;
use app\models\product\Product;
use app\models\productcategory\ProductCategory;
use app\models\productseo\ProductSeo;
use app\service\CategoryService;
use app\service\ProductService;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class ProductForm extends Model
{
	public $id;
	public $user_id;
	public $title;
	public $description;
	public $price;
	public $is_active;
	public $views = 0;
	public $category_id;
	public $category_name;

	public $seo_title;
	public $seo_description;
	public $seo_keyword;
	public $seo_url;

	public $images;

	/**
	 * @return array
	 */
	public function rules(): array
	{
		return [
			[['title', 'description', 'price', 'is_active'], 'required'],
			[['title', 'description', 'seo_title', 'seo_description', 'seo_url'], 'string'],
			[['is_active', 'price', 'id', 'category_id'], 'integer'],
			[['seo_keyword'], 'safe'],
			[['images'], 'file', 'maxFiles' => 10, 'skipOnEmpty' => true],
		];
	}

	public function init(): void
	{
		parent::init();
		$this->user_id = \Yii::$app->user->id;
	}

	/**
	 * @return bool
	 */
	public function add(): bool
	{
		$product = ProductService::get();

		/** @var Product $product */
		if (isset($product) && $product->load(['Product' => $this->attributes]) && $product->save()) {
			$this->attachCategory($product->id);
			$this->id = $product->id;
			$this->createSeo($product->id);
			return true;
		}

		return false;
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public function update($id): bool
	{
		$this->getSeo($id);
		$this->getCategory($id);
		return $this->load(['ProductForm' => ArrayHelper::toArray(Product::findOne($id))]);
	}

	/**
	 * @param $product_id
	 */
	private function createSeo($product_id): void
	{
		$seo = new ProductSeo([
			'product_id' => $product_id,
			'title' => $this->seo_title,
			'description' => $this->seo_description,
			'url' => $this->seo_url,
		]);

		$seo->save();
		$this->createKeywords($seo->id);
	}

	/**
	 * @param $seo_id
	 */
	private function createKeywords($seo_id): void
	{
		foreach ($this->seo_keyword as $keyword) {
			if ((int)$keyword !== 0) {
				$this->addRelation($keyword, $seo_id);
			} else {
				$model = new Keyword(['name' => $keyword]);

				$model->save();
				$this->addRelation($model->id, $seo_id);
			}
		}

	}

	/**
	 * @param $keyword_id
	 * @param $product_seo_id
	 */
	private function addRelation($keyword_id, $product_seo_id): void
	{
		$model = new ProductSeoKeyword([
			'keyword_id' => $keyword_id,
			'product_seo_id' => $product_seo_id,
		]);

		$model->save();
	}

	/**
	 * @param $ids
	 *
	 * @return \app\models\activerecord\keyword\ProductSeoQuery
	 */
	private function getKeywords($ids): \app\models\activerecord\keyword\ProductSeoQuery
	{
		return Keyword::find()->where(['id' => $ids]);
	}

	/**
	 * @param $product_id
	 */
	private function getSeo($product_id): void
	{
		$seo = ProductSeo::findOne(['product_id' => $product_id]);

		if ($seo !== null) {
			$this->seo_title = $seo->title;
			$this->seo_description = $seo->description;
			$this->seo_url = $seo->url;
			$this->seo_keyword = $this->getRelationKeywords($seo->id)->all();
		}
	}

	/**
	 * @param $seo_id
	 *
	 * @return \app\models\activerecord\keyword\ProductSeoQuery
	 */
	private function getRelationKeywords($seo_id): \app\models\activerecord\keyword\ProductSeoQuery
	{
		$ids = [];
		$models = ProductSeoKeyword::find()->where(['product_seo_id' => $seo_id]);

		foreach ($models->all() as $model) {
			$ids[] = $model->keyword_id;
		}

		return $this->getKeywords($ids);
	}

	/**
	 * @param $product_id
	 * @param $category_id
	 *
	 * @return bool
	 */
	private function attachCategory($product_id): bool
	{
		if ( $product_id !== null && $this->category_id !==null) {
			foreach ($this->category_id as $category) {
				$product_category = CategoryService::getJunctionPc(['product_id' => $product_id, 'pc_category_id' => $category]);

				$product_category->save();
			}

			return true;
		}

		return false;
	}

	/**
	 * @param $product_id
	 */
	private function getCategory($product_id): void
	{
		$models = ProductCategory::find()->where(['product_id' => $product_id])->all();

		foreach ($models as $model ) {
			$this->category_id[] = $model->pc_category_id;
		}
	}
}