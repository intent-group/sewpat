<?php

namespace app\service;


use app\models\pccategory\Category;
use app\models\productcategory\ProductCategory;
use yii\helpers\ArrayHelper;

class CategoryService
{
	/**
	 * @param null $id
	 *
	 * @return Category|null
	 */
	public static function get($id = null): ?Category
	{
		if ($id !== null) {
			return Category::findOne(['id' => $id]);
		}

		return new Category();
	}

	/**
	 * @param array $config
	 *
	 * @return ProductCategory
	 */
	public static function getJunctionPc(array $config): ProductCategory
	{
		return new ProductCategory($config);
	}

	/**
	 * @return array
	 */
	public static function toArray(): array
	{
		return ArrayHelper::map(Category::find()->all(), 'id', 'name');
	}
}