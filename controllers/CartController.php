<?php

namespace app\controllers;

use yii\web\Response;
use yii\web\Controller;

class CartController extends Controller
{
	/**
	 * @return bool
	 */
	public function actionAdd(): bool
	{
		if (\Yii::$app->request->post('id') !== null) {
			\Yii::$app->shopCart->addItem([\Yii::$app->request->post('id')]);

			return true;
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function actionAll()
	{
		return \Yii::$app->shopCart->allItems;
	}

	/**
	 * @return bool
	 */
	public function actionRemove(): bool
	{
		if (\Yii::$app->request->post('id') !== null) {
			\Yii::$app->shopCart->rremoveItem(\Yii::$app->request->post('id'));

			return true;
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function actionRemoveAll()
	{
		return \Yii::$app->shopCart->emptyShoppingCart();
	}

	/**
	 * @return mixed
	 */
	public function actionGetBasket()
	{
		return \Yii::$app->shopCart->basket();
	}

	/**
	 * @param $action
	 *
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		\Yii::$app->response->format = Response::FORMAT_JSON;
		return parent::beforeAction($action);
	}
}