<?php

namespace app\models\activerecord\discount;

use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property int $id
 * @property string $title
 * @property string $percent
 *
 * @property UserDiscount[] $userDiscounts
 * @property User[] $users
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['percent'], 'number'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'percent' => Yii::t('app', 'Percent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDiscounts()
    {
        return $this->hasMany(UserDiscount::className(), ['discount_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_discount', ['discount_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return DiscountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiscountQuery(get_called_class());
    }
}
