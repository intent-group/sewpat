<?php

namespace app\models\activerecord\productcategory;

use Yii;
use app\models\pccategory\Category;
use app\models\product\Product;

/**
 * This is the model class for table "product_pc_category".
 *
 * @property int $product_id
 * @property int $pc_category_id
 *
 * @property PcCategory $pcCategory
 * @property Product $product
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_pc_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'pc_category_id'], 'required'],
            [['product_id', 'pc_category_id'], 'integer'],
            [['product_id', 'pc_category_id'], 'unique', 'targetAttribute' => ['product_id', 'pc_category_id']],
            [['pc_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['pc_category_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'pc_category_id' => Yii::t('app', 'Pc Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'pc_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductPcCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductPcCategoryQuery(get_called_class());
    }
}
