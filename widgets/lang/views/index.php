<?php

/* @var $this \yii\web\View */
/* @var $current \app\models\activerecord\lang\Lang|\app\models\lang\Lang|array|null */
/* @var $langs \app\models\activerecord\lang\Lang[]|\app\models\lang\Lang[]|array */
use yii\helpers\Html;
?>
<div id="lang">
    <span id="current-lang">
        <?= $current->name;?> <span class="show-more-lang">▼</span>
    </span>
	<ul id="langs">
		<?php foreach ($langs as $lang):?>
			<li class="item-lang">
				<?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
			</li>
		<?php endforeach;?>
	</ul>
</div>