<?php

namespace app\service;


use app\models\image\Image;
use yii\base\DynamicModel;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Class ImageService
 * @package app\service
 */
class ImageService
{

	/**
	 * @var string $basePath
	 */
	private static $basePath = 'image/';

	/**
	 * @var string $galleryBasePath
	 */
	private static $galleryBasePath = 'image/gallery/';

	/**
	 * @param $model
	 * @param UploadedFile $image
	 * @param $attrName
	 * @param string $basePath
	 *
	 * @return bool
	 */
	public static function uploadImage($model, UploadedFile $image, $attrName, $basePath = null): bool
	{
		if ($basePath !== null) {
			self::$basePath = $basePath;
		}

		$dynamicModal = self::getValidator($image, ['skipOnEmpty' => false, 'extensions' => 'png, jpg']);

		/** @var ActiveRecord $model */
		if ($dynamicModal->validate()) {
			$model->$attrName = self::getNewName($image);
			$model->save();
			$image->saveAs(self::$basePath . $model->$attrName);
			return true;
		}

		return false;
	}

	/**
	 * @param $object_id
	 * @param array $images
	 * @param null $galleryBasePath
	 * @param string $attrName
	 *
	 * @return bool
	 */
	public static function uploadImages($object_id, array $images, $galleryBasePath = null, $attrName = 'path'): bool
	{
		if ($galleryBasePath !== null) {
			self::$galleryBasePath = $galleryBasePath;
		}

		$gallery_id = self::createGallery($object_id);

		foreach ($images as $picture) {
			$image = new Image(['gallery_id' => $gallery_id]);
			$dynamicModal = self::getValidator($picture, ['skipOnEmpty' => false, 'extensions' => 'png, jpg']);

			/** @var ActiveRecord $model */
			if ($dynamicModal->validate()) {
				$image->$attrName = self::getNewName($picture);
				$image->save();

				$picture->saveAs(self::$galleryBasePath . $image->$attrName);
			}
		}

		return true;
	}

	/**
	 * @param UploadedFile $image
	 *
	 * @return string
	 */
	private static function getNewName(UploadedFile $image): string
	{
		if (!empty($image->baseName)) {
			return $image->baseName . date('Y-m-D-H-m-s') . '.' . $image->extension;
		}

		return uniqid('', true) . date('Y-m-D-H-m-s') . '.' . $image->extension;
	}

	/**
	 * @param UploadedFile $image
	 * @param array $config
	 *
	 * @return DynamicModel
	 */
	private static function getValidator(UploadedFile $image, array $config): DynamicModel
	{
		$model = new DynamicModel(['image' => $image]);
		$model->addRule('image', 'file', $config);

		return $model;
	}

//	/**
//	 * @param $url
//	 */
//	private static function checkDir($url): void
//	{
//		if (!file_exists($url)) {
//			if (!mkdir($url) && !is_dir($url)) {
//				throw new \RuntimeException(sprintf('Directory "%s" was not created', $url));
//			}
//		}
//	}

	/**
	 * @param $product_id
	 *
	 * @return bool|int
	 */
	private static function createGallery($product_id)
	{
		$gallery = GalleryService::get();
		if (isset($gallery)) {
			$gallery->product_id = $product_id;
			if ($gallery->save()) {
				return $gallery->id;
			}
		}

		return false;
	}
}