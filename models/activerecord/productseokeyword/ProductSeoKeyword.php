<?php

namespace app\models\activerecord\productseokeyword;

use app\models\activerecord\keyword\Keyword;
use app\models\productseo\ProductSeo;
use Yii;

/**
 * This is the model class for table "product_seo_keyword".
 *
 * @property int $product_seo_id
 * @property int $keyword_id
 *
 * @property Keyword $keyword
 * @property ProductSeo $productSeo
 */
class ProductSeoKeyword extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_seo_keyword';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_seo_id', 'keyword_id'], 'required'],
            [['product_seo_id', 'keyword_id'], 'integer'],
            [['product_seo_id', 'keyword_id'], 'unique', 'targetAttribute' => ['product_seo_id', 'keyword_id']],
            [['keyword_id'], 'exist', 'skipOnError' => true, 'targetClass' => Keyword::className(), 'targetAttribute' => ['keyword_id' => 'id']],
            [['product_seo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSeo::className(), 'targetAttribute' => ['product_seo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_seo_id' => Yii::t('app', 'Product Seo ID'),
            'keyword_id' => Yii::t('app', 'Keyword ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeyword()
    {
        return $this->hasOne(Keyword::className(), ['id' => 'keyword_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSeo()
    {
        return $this->hasOne(ProductSeo::className(), ['id' => 'product_seo_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductSeoKeywordQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductSeoKeywordQuery(get_called_class());
    }
}
