<?php

/* @var $this \yii\web\View */


/* @var $models  */
?>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php foreach ($models as $model): ?>
			<div class="col-lg-4" style="border: black inset 1px; margin: 10px">
				<div class="col-lg-10">
					<div class="col-md-4">
						<?= \yii\helpers\Html::img('image/test2019-04-Mon-08-04-12.jpg', ['style' =>  'max-width: 100%']) ?>
					</div>
					<div class="col-md-12">
						Name: <?= $model->title ?>
					</div>
					<div class="col-md-12">
						Price: <?= $model->price ?>
						<?=  \app\widgets\cart\CartWidget::widget(['id' => $model->id])?>
					</div>
				</div>
				<div class="col-md-2">
					<?= \yii\helpers\Html::button('buy') ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
