<?php

namespace app\service;


use app\models\gallery\Gallery;
use app\models\image\Image;
use yii\helpers\Html;

class ProductService
{
	/**
	 * @param null $id
	 *
	 * @return \app\models\product\Product|null
	 */
	public static function get($id = null): ?\app\models\product\Product
	{
		if ($id !== null) {
			return \app\models\product\Product::findOne(['id' => $id]);
		}

		return new \app\models\product\Product;
	}

	/**
	 * @param $product_id
	 *
	 * @return array
	 */
	public static function getGallery($product_id): array
	{
		$prev = [];
		$gallery = Gallery::findOne(['product_id' => $product_id]);

		if ($gallery !== null) {
			$images = Image::find()->where(['gallery_id' => $gallery->id])->all();
			foreach ($images as $image) {
				$prev[] = Html::img('http://sewpatt.loc/image/gallery/' . $image->path, ['style' => 'max-width: 100%;']);
			}
		}

		return $prev;
	}

}