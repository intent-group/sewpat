<?php

namespace app\components\cart\product;


interface ShoppingProduct
{
	public function getPriceWithDiscount();

}