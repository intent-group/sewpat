<?php

namespace app\models\activerecord\productseokeyword;

/**
 * This is the ActiveQuery class for [[ProductSeoKeyword]].
 *
 * @see ProductSeoKeyword
 */
class ProductSeoKeywordQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ProductSeoKeyword[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ProductSeoKeyword|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
