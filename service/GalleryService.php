<?php

namespace app\service;


use app\models\gallery\Gallery;

class GalleryService
{
	/**
	 * @param null $id
	 *
	 * @return Gallery|null
	 */
	public static function get($id = null): ?Gallery
	{
		if ($id !== null) {
			return Gallery::findOne(['id' => $id]);
		}

		return new Gallery();
	}
}