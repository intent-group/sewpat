<?php

namespace app\service;


use app\models\user\User;
use yii\helpers\Html;
use yii\helpers\Url;

class MailService
{
	public static function sendCodeSignUp($code): bool
	{
		$user = UserService::get(\Yii::$app->user->id);

		\Yii::$app->mailer->compose()
			->setFrom('sewpatt@loc.lo')
			->setTo(['itsmymail8898@gmail.com' => 'root'])
			->setSubject('Registration complete on Sewpatt')
			->setTextBody('I ebal takie message')
			->setHtmlBody(self::setLink($code))
			->send();

		return true;
	}

	private static function setLink($code): string
	{
		return Html::a('Ne trogai silku, suka!',  Url::to(['sign-up-form/final-up', 'code' => $code]));
	}
}