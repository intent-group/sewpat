<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
	    'language'=>'ru-RU',
	    'i18n' => [
		    'translations' => [
			    '*' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@app/messages',
				    'sourceLanguage' => 'en',
				    'fileMap' => [
					    //'main' => 'main.php',
				    ],
			    ],
		    ],
	    ],
    	'reCaptcha' => [
			    'name' => 'reCaptcha',
			    'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
			    'siteKey' => '6Lcw55oUAAAAAEdXk-8X07rVkNq8VOXNkrkO6C_U',
			    'secret' => '6Lcw55oUAAAAAJYSVy8U1ASAwxZKjyDAGoxcQ4Bb',
	    ],
	    'shopCart' => [
	        'class' => \app\components\cart\ShopCart::class,
		    'calculateClass' => \app\components\cart\calculator\Calculator::class,
		    'storageClass' => \app\components\cart\storage\DbStorage::class,
		    'productClass' => \app\models\product\Product::class,
		    'table' => 'cart',
	    ],
        'request' => [
        	'class' => app\components\LangRequest::class,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'E6kEC8JTdoVjLOurja5TsyOpNQbahpGn',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\user\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
	    'urlManager' => [
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
		    'class'=>'app\components\LangUrlManager',
		    'rules'=>[
			    '/' => 'site/index',
			    '<controller:\w+>/<action:\w+>/'=>'<controller>/<action>',
		    ]
	    ],
    ],
	'modules' => [
		'profile' => [
			'class' => 'app\modules\profile\Module',
		],
	],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
